http = require('http')
server = http.createServer()

server.on('request', function(req, res) {
  console.log("new request from " + req.url)

  console.log(req.headers)

  req.on('data', function(data) {
    console.log('got data: ' + data.toString())
  })
  req.on('end', function() {
    res.writeHead(200)
    res.write('yo dawg')
    res.end()
  })
})
server.listen(8080)
